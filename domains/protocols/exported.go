package protocols

import (
	// local
	"gitlab.com/pztrn/gamebot/domains/protocols/irc"
	"gitlab.com/pztrn/gamebot/internal/configuration"
	"gitlab.com/pztrn/gamebot/internal/logger"

	// other
	"github.com/rs/zerolog"
)

var (
	log      zerolog.Logger
	protocol string
)

func Initialize() {
	log = logger.Logger.With().Str("domain", "protocols").Logger()
	log.Info().Msg("Initializing protocols...")

	irc.Initialize()
}

func Shutdown() {
	switch protocol {
	case "irc":
		irc.Shutdown()
	}
}

func Start() {
	log.Info().Str("protocol to use", configuration.Cfg.Protocols.ProtocolToUse).Msg("Starting protocol handler...")
	protocol = configuration.Cfg.Protocols.ProtocolToUse

	switch configuration.Cfg.Protocols.ProtocolToUse {
	case "irc":
		irc.Start()
	}
}
