package irc

import (
	// stdlib
	"crypto/tls"

	// local
	"gitlab.com/pztrn/gamebot/internal/configuration"
	"gitlab.com/pztrn/gamebot/internal/logger"

	// other
	"github.com/rs/zerolog"
	"github.com/thoj/go-ircevent"
)

var (
	log zerolog.Logger

	conn *irc.Connection
)

func Initialize() {
	log = logger.Logger.With().Str("package", "protocols").Str("protocol", "IRC").Logger()
	log.Info().Msg("Initializing protocol handler...")
}

func Shutdown() {
	log.Info().Msg("Shutting down connection to IRC server...")

	if channelJoined {
		conn.Part(configuration.Cfg.Protocols.IRC.Channel)
	}
	conn.Quit()
}

func Start() {
	log.Info().Str("server", configuration.Cfg.Protocols.IRC.Server).Str("username", configuration.Cfg.Protocols.IRC.Username).Str("nick", configuration.Cfg.Protocols.IRC.Nick).Msg("Connecting to server...")

	// IRC connection creation.
	conn = irc.IRC(configuration.Cfg.Protocols.IRC.Nick, configuration.Cfg.Protocols.IRC.Username)
	conn.UseTLS = configuration.Cfg.Protocols.IRC.Secure
	if configuration.Cfg.Protocols.IRC.DoNotValidateCertificate {
		conn.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	}

	conn.AddCallback("*", eventHandler)

	err := conn.Connect(configuration.Cfg.Protocols.IRC.Server)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to connect to IRC server!")
	}

	conn.Join(configuration.Cfg.Protocols.IRC.Channel)
}
