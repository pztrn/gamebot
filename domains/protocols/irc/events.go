package irc

import (
	// local
	"gitlab.com/pztrn/gamebot/internal/configuration"
	"gitlab.com/pztrn/gamebot/internal/logger"

	// other
	"github.com/thoj/go-ircevent"
)

var (
	// First MODE was received. Indicates that we can join channels.
	firstMODEreceived bool
	// We have joined our channel.
	channelJoined bool
)

// The very main event handler.
func eventHandler(event *irc.Event) {
	if logger.SuperVerbosive {
		log.Debug().Msgf("Received event from IRC server: %+v", event)
	}

	// First MODE received. This indicates that we have successfully
	// logged in and can join channels.
	if event.Code == "MODE" && !firstMODEreceived {
		firstMODEreceived = true
		conn.Join(configuration.Cfg.Protocols.IRC.Channel)
		channelJoined = true
	}
}
