package commandshandler

import (
	// local
	"gitlab.com/pztrn/gamebot/internal/message"
)

type CommandHandler struct {
	// Command to handle. Should be in first word in message with
	// command character specified in configuration file. For example,
	// if command character is "!" and you want "!ping" command, then
	// this field should be just "ping".
	// When checking for command this field will be lowercased.
	Command string
	// Handler is a function that handles message. It should return a
	// string that we should send back to channel (or private chat or
	// whatever this message came from). If no response required - string
	// should be empty.
	Handler func(message *message.Message) string
}
