package commandshandler

import (
	// local
	"gitlab.com/pztrn/gamebot/internal/logger"

	// other
	"github.com/rs/zerolog"
)

var (
	log zerolog.Logger
)

func Initialize() {
	log = logger.Logger.With().Str("package", "commandshandler").Logger()
	log.Info().Msg("Initializing package...")
}
