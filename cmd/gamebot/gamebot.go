package main

import (
	// stdlib
	"flag"
	"os"
	"os/signal"
	"syscall"

	// local
	"gitlab.com/pztrn/gamebot/domains/commandshandler"
	"gitlab.com/pztrn/gamebot/domains/protocols"
	"gitlab.com/pztrn/gamebot/internal/configuration"
	"gitlab.com/pztrn/gamebot/internal/logger"
)

func main() {
	logger.Initialize()

	logger.Logger.Info().Msg("Starting GameBot by pztrn. Version <put_it_here>")
	logger.Logger.Info().Msg("Copyright (c) 2019, Stanislav N. aka pztrn.")
	logger.Logger.Info().Msg("https://gitlab.com/pztrn/gamebot")

	configuration.Initialize()
	protocols.Initialize()
	commandshandler.Initialize()

	flag.Parse()

	configuration.Load()
	protocols.Start()

	// CTRL+C handler.
	signalHandler := make(chan os.Signal, 1)
	shutdownDone := make(chan bool, 1)
	signal.Notify(signalHandler, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-signalHandler
		protocols.Shutdown()
		shutdownDone <- true
	}()

	<-shutdownDone
	os.Exit(0)

}
