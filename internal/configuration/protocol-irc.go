package configuration

// IRC represents IRC protocol configuration structure.
type IRC struct {
	// Server is a server string we will use for connection. Should be
	// in form of "addr:port".
	Server string `yaml:"server"`
	// Secure shows us that we should use secure connection (SSL/TLS).
	Secure bool `yaml:"secure"`
	// DoNotValidateCertificate shows us that we should not validate
	// certificate while using secure connection. Does nothing for
	// insecure connection.
	DoNotValidateCertificate bool `yaml"do_not_validate_certificate"`
	// Username is a part of handle before "@".
	Username string `yaml:"username"`
	// Nick is a nickname bot will use on IRC.
	Nick string `yaml:"nick"`
	// Channel bot will join.
	Channel string `yaml:"channel"`
}

func (i *IRC) CheckAndLoadDefaults() {
	log.Debug().Msg("Checking IRC protocol configuration...")

	if i.Server == "" {
		log.Warn().Msg("Server wasn't specified, using 127.0.0.1:6667")
		i.Server = "127.0.0.1:6667"
	}

	if i.Username == "" {
		log.Warn().Msg("Username wasn't specified, using 'gamebot'")
		i.Username = "gamebot"
	}

	if i.Nick == "" {
		log.Warn().Msg("Nick wasn't specified, using 'gamebot'")
		i.Nick = "gamebot"
	}

	if i.Channel == "" {
		log.Fatal().Msg("Channel isn't specified, bot will not join any channel! Please specify channel to join!")
	}
}
