package configuration

// Config represents gamebot configuration structure.
type Config struct {
	Protocols Protocols `yaml:"protocols"`
}

// CheckAndLoadDefaults checks loaded and parsed configuration and
// loads sane defaults if nothing was parsed from file.
func (c *Config) CheckAndLoadDefaults() {
	log.Info().Msg("Checking configuration file for validity and load defaults if needed...")

	c.Protocols.CheckAndLoadDefaults()
}
