package configuration

// Protocols represents protocols configuration structure.
type Protocols struct {
	// ProtocolToUse is a protocol name to use. Only one protocol
	// can be used simultaneously ATM.
	ProtocolToUse string `yaml:"protocol_to_use"`
	// IRC is an IRC protocol configuration.
	IRC IRC `yaml:"irc"`
}

func (p *Protocols) CheckAndLoadDefaults() {
	log.Debug().Msg("Checking protocols configuration...")

	if p.ProtocolToUse == "" {
		log.Warn().Msg("Default protocol (parameter 'protocol_to_use') is empty, forcing IRC")
		p.ProtocolToUse = "irc"
	}

	p.IRC.CheckAndLoadDefaults()
}
