package configuration

import (
	// stdlib
	"flag"
	"io/ioutil"
	"path/filepath"
	"strings"

	// local
	"gitlab.com/pztrn/gamebot/internal/logger"

	// other
	"github.com/rs/zerolog"
	"gopkg.in/yaml.v2"
)

var (
	Cfg               *Config
	configFilePathRaw string
	log               zerolog.Logger
)

// Initialize initializes package with own logger and reads configuration
// from file.
func Initialize() {
	log = logger.Logger.With().Str("package", "configuration").Logger()
	log.Info().Msg("Initializing subsystem...")

	flag.StringVar(&configFilePathRaw, "conf", "", "Configuration file path. Should be absolute.")
}

// Load loads configuration from file and parses it into Config structure.
func Load() {
	if configFilePathRaw == "" {
		log.Fatal().Msg("Configuration file path wasn't defined!")
	}

	log.Info().Str("configuration file path", configFilePathRaw).Msg("Loading configuration from file...")

	// While this replacement can be done I don't want to bother with
	// it, sorry. Really, specify full file path to avoid any type of
	// confisuion. All merge requests regarding this will be closed.
	if strings.Contains(configFilePathRaw, "~") {
		log.Fatal().Msg("Found tilde (~) in configuration file path. Please, specify absolute file path!")
	}

	configFilePath, err := filepath.Abs(configFilePathRaw)
	if err != nil {
		log.Error().Err(err).Msg("Failed to get really absolute configuration file path!")
	}

	configFileData, err1 := ioutil.ReadFile(configFilePath)
	if err1 != nil {
		log.Fatal().Err(err1).Msg("Failed to read configuration file contents!")
	}

	Cfg = &Config{}
	err2 := yaml.Unmarshal(configFileData, &Cfg)
	if err2 != nil {
		log.Fatal().Err(err2).Msg("Failed to parse configuration file!")
	}

	Cfg.CheckAndLoadDefaults()

	log.Info().Msg("Configuration file loaded")
	log.Debug().Msgf("%+v", Cfg)
}
