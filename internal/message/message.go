package message

// Message represents generic message that was received and parsed.
type Message struct {
	// Protocol from which message was received.
	Protocol string
	// Channel identificator. "#channame" for IRC, "room@domain" for
	// XMPP, etc.
	Channel string
	// Nickname of user that sent this message.
	Nickname string
	// Handle is a protocol-specific handle. Can be "user@addr" for IRC,
	// "user@domain" for XMPP, etc.
	Handle string
	// Actual message
	Message string
}
